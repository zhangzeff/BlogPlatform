package com.group6.blogplatform.service.inter;

import com.group6.blogplatform.Bean.Comment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {
    List<Comment> listcommentByBlogId(Long id);

    Comment saveComment(Comment comment);
}
