package com.group6.blogplatform.service.inter;

import com.group6.blogplatform.Bean.User;

public interface UserService {

    User checkUser(String username, String password);

    boolean addUser(User user);
}
