package com.group6.blogplatform.service.impl;

import com.group6.blogplatform.Bean.Type;
import com.group6.blogplatform.NotFoundExcepiton;
import com.group6.blogplatform.dao.TypeDao;
import com.group6.blogplatform.service.inter.TypeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeDao typeDao;

    @Transactional
    @Override
    public Type saveType(Type type) {
        return typeDao.save(type);
    }

    @Transactional
    @Override
    public Type getType(Long id) {
        return typeDao.getOne(id);
    }

    @Override
    public Type getTypeByTypeName(String typeName) {
        return typeDao.findByTypeName(typeName);
    }

    @Transactional
    @Override
    public Page<Type> listType(Pageable pageable) {
        return typeDao.findAll(pageable);
    }

    @Override
    public List<Type> listType() {
        return typeDao.findAll();
    }

    @Override
    public List<Type> listTypeTop(Integer size) {
        Pageable pageable = PageRequest.of(0, size, Sort.by(Sort.Direction.DESC, "blogs.size"));
        return typeDao.findTop(pageable);
    }

    @Override
    public Type updateType(Long id, Type type) {
        Type type1 = typeDao.getOne(id);
        if (type1==null){
            throw new NotFoundExcepiton("不存在该类型");
        }
        BeanUtils.copyProperties(type, type1);
        return typeDao.save(type1);
    }

    @Override
    public void deleteType(Long id) {
        typeDao.deleteById(id);
    }
}
