package com.group6.blogplatform.service.impl;

import com.group6.blogplatform.Bean.User;
import com.group6.blogplatform.dao.UserDao;
import com.group6.blogplatform.service.inter.UserService;
import com.group6.blogplatform.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;



    @Override
    public User checkUser(String username, String password) {
        User user = userDao.findByUsernameAndPassword(username, MD5Utils.code(password));
        return user;
    }

    @Transactional
    @Override
    public boolean addUser(User user) {
        userDao.save(user);
        return true;
    }
}
