package com.group6.blogplatform.dao;

import com.group6.blogplatform.Bean.Type;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface TypeDao extends JpaRepository<Type,Long> {
    Type findByTypeName(String TypeName);


    @Query("select t from Type t")
    List<Type> findTop(Pageable pageable);
}

