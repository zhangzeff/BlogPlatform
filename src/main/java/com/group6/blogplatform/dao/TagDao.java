package com.group6.blogplatform.dao;

import com.group6.blogplatform.Bean.Tag;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface TagDao extends JpaRepository<Tag,Long> {

    Tag findByTagName(String TagName);

    @Query("select t from Tag t")
    List<Tag> findTop(Pageable pageable);
}
