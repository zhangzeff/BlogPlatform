package com.group6.blogplatform.dao;

import com.group6.blogplatform.Bean.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User,Long> {

    User findByUsernameAndPassword(String username, String password);
}
